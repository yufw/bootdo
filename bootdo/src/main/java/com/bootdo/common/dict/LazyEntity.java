package com.bootdo.common.dict;

import java.io.Serializable;

public interface LazyEntity extends Serializable {
    Object get();
}
