package com.bootdo.common.dict;
import com.bootdo.common.config.DruidDBConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
/**
 * @ClassName TailBean
 * @Description //TODO
 * @Author 武玉锋
 * @Date 2019-07-29 15:52:
 **/
public class TailBean implements Tail {
    private Logger logger = LoggerFactory.getLogger(DruidDBConfig.class);
    protected Map<String, Object> extMap = new HashMap();
    boolean hasLazy = false;

    public TailBean() {
    }

    public Object get(String key)  {
        if (this.hasLazy) {
            Object o = this.extMap.get(key);
            if (o instanceof LazyEntity) {
                LazyEntity lazyEntity = (LazyEntity)o;

                try {
                    Object real = lazyEntity.get();
                    this.extMap.put(key, real);
                    return real;
                } catch (RuntimeException var5) {
                    logger.error("Lazy Load Error:" + key + "," + var5.getMessage());
                    return new Exception();
                }
            } else {
                return o;
            }
        } else {
            return this.extMap.get(key);
        }
    }

    public void set(String key, Object value) {
        if (value instanceof LazyEntity) {
            this.hasLazy = true;
        }

        this.extMap.put(key, value);
    }

    public Map<String, Object> getTails() {
        Map<String, Object> newExtMap = new HashMap();
        if (this.hasLazy) {
            Iterator var2 = this.extMap.entrySet().iterator();

            while(var2.hasNext()) {
                Map.Entry<String, Object> entry = (Map.Entry)var2.next();
                String key = (String)entry.getKey();
                Object value = entry.getValue();
                if (value instanceof LazyEntity) {
                    try {
                        LazyEntity lazyEntity = (LazyEntity)value;
                        Object real = lazyEntity.get();
                        newExtMap.put(key, real);
                    } catch (RuntimeException var8) {
                        logger.error("Lazy Load Error:" + key + "," + var8.getMessage());

                    }
                } else {
                    newExtMap.put(key, value);
                }
            }

            this.extMap = newExtMap;
            this.hasLazy = false;
        }

        return this.extMap;
    }
}
