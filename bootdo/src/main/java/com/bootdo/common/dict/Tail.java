package com.bootdo.common.dict;

import java.io.Serializable;

public interface Tail extends Serializable {
    Object get(String var1) throws Exception;

    void set(String var1, Object var2);
}
